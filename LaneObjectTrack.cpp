//name:LaneObjectTrack
#include <iostream>
#include <cmath>
#include <math.h>
#include "vehicleModelClass.h"
#include "LaneObjectTrack.h"
#include "team3/Obstacles.h"
using namespace std;

//input object collision avoidance
static double xObjPosL;
static double yObjPosL;
static double xObjPosR;
static double yObjPosR;

//input lane marking
static double mag,nx,ny;//magnitude
static double xDest,yDest;//coor of destination centre point
static double xSource,ySource;//coor of source centra point
static double xPos,yPos;//car current postion
static double headAngle;//car heading angle

//calc lane marking
static double destAngle;//source point to destination point angle
static double driftdir;//the angle difference between car and destination
static double driftdist;//the distance between car and destination
static double velocity;//car current velocity
static double headAngleCorrect;//the correct heading angle for the car

//actuators
static double brakePower;
static double steeringAngle;
//vehicle model (toyota vios)
static double bodyWidth=1.7;//1.7 meter for car body size(left+right)
static double bodyLength=4.41;
static double roadWidth=3.5;//3.5 meter standard malaysia highway road width
static double carToLaneDist;
static double mass;
static double gravity=9.81;
static double coeff=0.8;
static double objDetectHold=5;//threshold value of distance of obstacle detection

//calculated side object distance
static double leftObjDist;//final left obj dist
static double rightObjDist;//final right obj dist
static double objDistL=objDetectHold;//initiate with max detect distance
static double objDistR=-objDetectHold;//initiate with max detect distance(negative right)
static double objAngleL;
static double objAngleR;

//flag indicate that car is deviated
static bool isLeftDev;
static bool isRightDev;

//flag that indicate that object is near the car;
static bool isLeftObj;
static bool isRightObj;

//counter
static int objCount;

static void calcObjDist(){
	double bLen=bodyLength/2;//front(+)/back(-) body length
	double bWid=bodyWidth/2;//left(+) right(-)

	double xL=xObjPosL-xPos;//position of x after transform (objPos-carPos)
	double yL=yObjPosL-yPos;//position of y after transform (objPos-carPos)
	double xR=xObjPosR-xPos;//position of x after transform (objPos-carPos)
	double yR=yObjPosR-yPos;//position of y after transform (objPos-carPos)
	//cout<<"xL:"<<xL<<",yL:"<<yL<<",xR:"<<xR<<",yR:"<<yR<<endl;//debug
	double objDistLtemp=objDistL;
	double objDistRtemp=objDistR;

	objAngleL=atan2((double)(yL),(xL))*57.29578 -(headAngle*57.29578);//object that located on left side
	//cout<<"left object angle:"<<objAngleL<<endl;
	if(objAngleL>21.04 && objAngleL<158.96){//only display the angle when the object is on LR side
		if( xL<bLen && xL>-bLen ){//objectPosX is less than 2.21 and more than -2.21
			objDistL=yL-bWid;
		}
		else{//objectPosx is more than 2.21
			objDistL=sqrt((pow((xL),2)+pow((yL),2)))-(bWid/cos(objAngleL));
		}
	}
	else
		objDistL=objDetectHold;//assume a case that there is no obstacle

	objAngleR=atan2((double)(yR),(xR))*57.29578-(headAngle*57.29578);//object that located on left side
	//cout<<"right Object angle:"<<objAngleR<<endl;
	if(objAngleR<-21.04 && objAngleR>-158.96){//only display the angle when the object is on LR side
		if( xR<bLen && xR>-bLen ){//objectPosX is less than 2.21 and more than -2.21
			objDistR=yR+bWid;
		}
		else{//objectPosx is more than 2.21
			//cout<<"distance:"<<-sqrt((pow((xR),2)+pow((yR),2)))<<"+"<<(bWid/cos(objAngleR))<<endl;
			objDistR=-sqrt((pow((xR),2)+pow((yR),2)))+(bWid/cos(objAngleR));
		}
	}
	else
		objDistR=-objDetectHold;//assume a case that there is no obstacle

	if(objDistL<objDistLtemp && objDistL<objDetectHold){
		leftObjDist=objDistL;
	}
	else{
		leftObjDist=objDistLtemp;
	}

	if(objDistR>objDistRtemp && objDistR>-objDetectHold){
		rightObjDist=objDistR;
	}
	else{
		rightObjDist=objDistRtemp;
	}
	//cout<<"left Object distance:"<<leftObjDist<<endl;
	//cout<<"right Object distance:"<<rightObjDist<<endl;
}

static void calcLaneDist(){
	mag = sqrt(pow(xDest - xSource, 2) + pow(yDest - ySource, 2)); //magnitude of path's vector
	nx = (xDest - xSource) / mag;
	ny = (yDest - ySource) / mag;
	//cout<<"mag:"<<mag<<endl<<"nx:"<<nx<<endl<<"ny:"<<ny<<endl;
	//cout<<"yDest:"<<yDest<<" yPos:"<<yPos<<" xDest:"<<xDest<<" xPos"<<xPos<<endl;
	destAngle=atan2(yDest - yPos, xDest - xPos) * 57.29578;
	//cout<<"angle from car to destination point:"<<destAngle<<endl;
	// tune the head angle according to the distance from the line!
	// use cross product to find the angle that it should turn (right hand rule)
	// formula: sin theta=|(dest-source)CROSS(carPos-source)|/ (|dest-source|*|carPos-source|)
	mag = sqrt(pow(xDest - xSource, 2) + pow(yDest - ySource, 2));
	mag *= sqrt(pow(xPos - xSource, 2) + pow(yPos - ySource, 2));
	//cout<<"mag second:"<<mag<<endl;
	driftdir = ((xDest - xSource) * (yPos - ySource) - (yDest - ySource) * (xPos - xSource));
	if(mag!=0){
		driftdir /= mag;
	}
	else{
		driftdir=0;
	}
	//cout<<"drift direction"<<driftdir<<endl;
	//asin(driftdir) is the angle between the line formed by source-dest and line formed by source-car.
	//this formula calculate the shortest distance between the car and the line
	driftdist = driftdir * sqrt(pow(xPos - xSource, 2) + pow(yPos - ySource, 2));
	//cout<<"distance drifted from the centre of lane:"<<driftdist<<endl;
	carToLaneDist=(roadWidth/2)-(bodyWidth/2)-(abs(driftdist));
	//cout<<"car to lane shortest distance:"<<carToLaneDist<<endl;
}

static void calcSteerAngle(){
	//static double integral=0;
	//static double lastDrift=0;
	//double deriv;
    if(isLeftObj)//if left object too near to the car,move steering angle slightly right
    	steeringAngle-=0.1;//move steering to left
    else if(isRightObj)//if right object near to the car,move steering angle slightly left
    	steeringAngle+=0.1;//move steering to the right


    if(isLeftDev||isRightDev)//if either left/right deviation, execute this code
    {
    	// check if the robot is far/near from the line:
        if (abs(driftdist) > 0.2) {       //DEBUG : RISK OF DIVISION BY ZERO WHEN VELOCITY IS 0:
            headAngleCorrect = atan2(yDest - ySource, xDest - xSource) * 57.29578 - atan(driftdist/0.5)* 57.29578 *( 9/(abs(velocity)+11)*0 + 1); //TODO add a /sin(angleerror)
        }

        steeringAngle=headAngleCorrect*0.006*(exp(abs(driftdist*1))-1);
        if(abs(driftdist)>0.3) steeringAngle+=headAngleCorrect*0.01;

        //deriv=(driftdist-lastDrift)*3; //debug use
        //lastDrift=driftdist; //debug use
        //steeringAngle+=deriv;//debug use

        steeringAngle = fmod(steeringAngle,360);
        if(steeringAngle>180){
        	steeringAngle-=360;
        }
        if(steeringAngle<-180){
        	steeringAngle+=360;
        }

        cout<<"correct head angle that the car should have:"<<steeringAngle<<"::"<<headAngleCorrect<<"::"<<headAngle<<endl;
    }
}

static void calcBrakePower(){
	double brakeDist;
	double oldVelocity;
	double newVelocity;
	double brakeForce;
	double maxbForce=16402.3;
	oldVelocity=velocity;//preload velocity
	brakeDist=pow(velocity,2)/(2*coeff*gravity);//formula used to calc safety stop distance
	//cout<<"brakeDisrt"<<brakeDist<<endl;//debug
	newVelocity=oldVelocity-(brakeDist/2);//2 second rule
	//cout<<"newVelocity"<<newVelocity<<endl;//debug
	brakeForce=mass*abs(((newVelocity-oldVelocity)/2));//brake force= mass* deceleration
	//cout<<"brake force"<<brakeForce<<endl;//debug
	brakePower=brakeForce/(maxbForce);//divided by max brake force
	if(brakePower<0.1)//brake should not zero, must always apply little brake
		brakePower=0.1;
	if(brakePower>1)//brake factor maximum limit is 1
		brakePower=1;
	//cout<<"brakePower:"<<brakePower<<endl;
}

static void actuatorControl(){
	//brake power (when car deviated)
	calcBrakePower();
	//steering control(when car deviated and object near)
	calcSteerAngle();
}

void calcLaneTrack(Vehicle &v, Vehicle &v_lanetrack){
	static bool firstTime=true;
	velocity=v.velocity;
	xPos=v.x;
	yPos=v.y;
	headAngle=v.heading*57.29578;
	mass=v.mass;
	if(firstTime==true){
		xSource=getRoadStartPos().x;
		ySource = getRoadStartPos().y;
		xDest = getRoadEndPos().x;
		yDest = getRoadEndPos().y;
	firstTime=false;
	}

	//load the obstacle and loop for finding the minimum distance of object
	for(int n=0;n<v.obstacleNum;n++){
		xObjPosL=v.obstacleList[n].x;
		yObjPosL=v.obstacleList[n].y;
		xObjPosR=v.obstacleList[n].x;
		yObjPosR=v.obstacleList[n].y;
		calcObjDist();//run object tracking algorithm
	}

	calcLaneDist();//run lane tracking algorithm

	//detect vehicle deviation algorithm
	if (driftdist>0 && carToLaneDist<0.65){//car is deviated to left lane
		isLeftDev=true;
		cout<<"car is deviated to left lane"<<endl;
	}else if(driftdist<0 && carToLaneDist<0.65){
		isRightDev=true;
		cout<<"car is deviated to right lane"<<endl;
	}else{
		//clear the deviated flag
		isLeftDev=false;
		isRightDev=false;
		cout<<"car is on lane"<<endl;
	}

	//action taken when car is deviated to left or right
	if((isLeftDev || isRightDev)){
		actuatorControl();
		v_lanetrack.brakePedal = brakePower*0.90;
		v_lanetrack.steeringAngle=steeringAngle/57.29578;//convert steering angle to radian
	}
	else if(!isLeftDev && !isRightDev){//driver manually corrected
		cout<<"car is on lane"<<endl;
	}

	//detect left/right object within range of 4.5 meter
	if(leftObjDist<objDetectHold && leftObjDist>0){
		isLeftObj=true;
		cout<<"there are an object on your left side"<<endl;
	}
	else
		isLeftObj=false;//clear the left object detection flag

	if(rightObjDist>-objDetectHold && rightObjDist<0){
		isRightObj=true;
		cout<<"there are an object on your right side"<<endl;
	}else
		isRightObj=false;//clear the right object detection flag

	//action taken when there is object get too close to the car
	if((isLeftObj || isRightObj) && objCount<5){
		objCount++;
		//beep the buzzer

	}else if(!isLeftObj && !isRightObj){
		objCount=0;
	}

	if(objCount==5){
		//25ms * 5 = 125ms then activate
		//automatic corrective action
		actuatorControl();
		v_lanetrack.steeringAngle=steeringAngle/57.29578;//convert steering angle to radian
		v_lanetrack.brakePedal=brakePower*0.90;
	}
//	calcSteerAngle();
//	v_lanetrack.steeringAngle=steeringAngle/57.29578;//convert steering angle to radian
}



