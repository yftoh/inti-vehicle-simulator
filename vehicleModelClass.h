/*
 * vehicleModelClass.h
 *
 *  Created on: Jun 9, 2014
 *      Author: tyf and QCB
 */

#ifndef VEHICLEMODELCLASS_H_
#define VEHICLEMODELCLASS_H_

#include "pointClass.h"

class Vehicle
{
public:
	double x; //x coordinate in meters
	double y; //y coordinate in meters
	double heading;  //heading angle in radians

	//output from our system: (to be applied to ACTUATORS)
	double accelPedal;
	double brakePedal;
	double steeringAngle;

	//input from user
	double userAccel;
	double userBrake;
	double userSteer;

	double mass;
	double acceleration; // in m/s/s
	double velocity; //in m/s

	float length;
	float width;
	float height;
	float ROW;
	float vMax;
	float Tengine;
	float gearRatio;
	float finalGearRatio;
	float fbMax;
	float WB;
	float track;

	//Toh Yi Feng
	Vehicle();
	Vehicle(double xPos,double yPos,double headingAngle);
	//Quah Chin Beng
	Vehicle(double xPos, double yPos,double headingAngle,double mass1,float length1,double width1,float height1,
			float ROW1,float vMax1,float Tengine1,float gearRatio1,float finalGearRatio1,float WB1,float track1);

	Point * obstacleList;
	int obstacleNum;
};


#endif /* VEHICLEMODELCLASS_H_ */
