/*
 * DecisionControl.h
 *
 *  Created on: Jul 7, 2014
 *      Author: soonzhi
 */

#ifndef DECISIONCONTROL_H_
#define DECISIONCONTROL_H_

void decisionControl(Vehicle &v, Vehicle &v_lanetrack,Vehicle &v_obsDetect);

#endif /* DECISIONCONTROL_H_ */
