/*
 * main.cpp
 *
 *  Created on: Jun 3, 2014
 *      Author: Goh Yan Xian
 */
#include <cmath>
#include <iostream>
using namespace std;
#include "vehicleModelClass.h"
#include "ObstacleDetect.h"

static double velocity;
static double headingAngle;
static double xF;
static double yF;
static double xB;
static double yB;
static double mass;
static double xCar;
static double yCar;
static double ObstacleAngle(double x,double y);
static double ObstacleDistanceFront(double x, double y,double distance);
//double ObstacleDistanceBack(double x, double y,double distance);
static double BrakePower(double dist);
static double AccePower(double dist);
static double DistanceFront=50;
static double backDistance=50;
static double DistanceBack=50;
static bool isBackObs;
static bool isFrontObs;

double ObstacleAngle(double x,double y){ //Calculate obstacle angle function
	return ((atan2(y,x)*57.29578)-(headingAngle*57.29578));
}
//double ObstacleDistanceBack(double x, double y,double distance){ //Calculate obstacle distance function
//	//transform of
//	double temp = distance;
//	double bDist;
//	double angle=0;
//	x=x-xCar;
//	y=y-yCar;
//	angle = ObstacleAngle(x,y);
//
//   if(xCar>xB){
//	   cout<<"xCar::xB "<<xCar<<"::"<<xB<<"::"<<angle<<endl;
//	   if((angle<=180.1 && angle>157)||(angle>=-180.1 && angle<-157))
//	   {
//		   bDist = sqrt(pow(x,2)+pow(y,2))-abs(2.21/cos(angle*3.142/180));
//		   bDist=abs(bDist);
//		   //cout<<"Distance:"<<dist<<" Angle:"<<angle<<endl;
//		   cout<<"Distance to Xb "<<bDist<<"::"<<temp<<endl;
//		   if(bDist<=(abs((2.21/cos(angle*3.142/180))))){
//			   return 0;
//		   }
//
//
//		}
//	   else
//		  return 300;
//
//	   if(bDist<temp){
//		   cout<<"return dist"<<endl;
//		   return bDist;
//	   }
//	   else{
//		   cout<<"return temp"<<endl;
//		   return temp;
//		}
//   }
//   else{
//	   return 0;
//   }
//
//
//}
double ObstacleDistanceFront(double x, double y,double distance){ //Calculate obstacle distance function
	//transform of
	double temp = distance;
	double dist;
	double angle;
	x=x-xCar;
	y=y-yCar;
	angle = ObstacleAngle(x,y);
	//cout<<"obstacle angle"<<angle<<endl;
	if(xCar<xF){
		//cout<<"xCar::xF "<<xCar<<"::"<<xF<<"::"<<angle<<endl;
		if((angle>=0 && angle<22)||(angle<=0 && angle>-22))
		{
			 //If obstacle at the front side
			  dist = sqrt(pow(x,2)+pow(y,2))-abs((2.21/cos(angle*3.142/180)));
			  //cout<<"Distance:"<<dist<<" Angle:"<<angle<<endl;
			  if(dist<=(abs((2.21/cos(angle*3.142/180)))))
			  {
				  return dist=0;
			  }

			  if(dist<temp){
				  return dist;
			  }
			  else{
				  return temp;
			  }
		}
	}
	else{
		return 300;
	}
}

void calcObstacleBack(){
	double x = xB-xCar;
	double y= yB-yCar;
	double temp = DistanceBack;
	double angle;
	angle = ObstacleAngle(x,y);
	cout<<"xCar::xB "<<xCar<<"::"<<xB<<"::"<<angle<<"::"<<temp<<endl;
	if((angle<=181 && angle>157)||(angle>=-181 && angle<-157))
	{
		backDistance=sqrt(pow(x,2)+pow(y,2))-abs(2.21/cos(angle*3.142/180));
		backDistance = abs(backDistance);
		cout<<"backDistance"<<backDistance<<endl;
	}
	else
	{
		backDistance=300;
	}

	if(backDistance<temp && backDistance<300){
		DistanceBack=backDistance;
	}
	else
	{
		DistanceBack=temp;
	}

	if(xCar>xB && (xCar-xB > 5) )
			DistanceBack=300;


}
double AccePower(double dist){//Accelerator power function used to activate speed with different distance from car to back obstacle

	double oldvelocity = velocity;
	double newvelocity = sqrt(dist*2*0.8*9.81);
	double aceleration = abs(((newvelocity-oldvelocity)/2));
	double mass =1045;
	double force = mass* aceleration;
	double maxforce=(oldvelocity/3)*1045;
	double stoppingDistance = (oldvelocity*oldvelocity)/(2*0.8*9.81);
	double acceFactor = force/maxforce;

	if(acceFactor>1)
		acceFactor=1;
	if(dist>stoppingDistance)
	{
		return 0;
	}

	else
	{
		return acceFactor;
	}
}


double BrakePower(double dist){	//Brake power function used to activate brake percent with different distance from car to front obstacle
	double oldvelocity = velocity;
	double newvelocity = sqrt(dist*2*0.8*9.81);
	double deceleration = abs(((newvelocity-oldvelocity)/2));
	double mass =1045;
	double force = mass* deceleration;
	double maxforce=(oldvelocity/3)*1045;
	double stoppingDistance = (oldvelocity*oldvelocity)/(2*0.8*9.81);
	double brakeFactor = force/maxforce;

	if(brakeFactor>1)
		brakeFactor=1;
	//cout<<"show distance"<<dist<<":"<<stoppingDistance<<"::"<<brakeFactor<<endl;
	if(dist>stoppingDistance){
			return 0;
	}

	else{
			return brakeFactor;
	}
}





void calcObstacleDetection(Vehicle &v, Vehicle &v_obsDetect){

	velocity=v.velocity;
	xCar=v.x;
	yCar=v.y;
	mass=v.mass;
	headingAngle=v.heading;


     for(int n=0;n<v.obstacleNum;n++){

    	 xF=v.obstacleList[n].x;
    	 yF=v.obstacleList[n].y;
    	 xB=v.obstacleList[n].x;
    	 yB=v.obstacleList[n].y;
    	 DistanceFront=ObstacleDistanceFront(xF,yF,DistanceFront);
    	 //DistanceBack=ObstacleDistanceBack(xB,yB,DistanceBack);
    	 calcObstacleBack();
     }
     cout<< "FRONT:"<<DistanceFront<<endl;
     cout<<"BACK: "<<DistanceBack<<endl;

     //cout<<"Shortest Distance back: "<<DistanceBack<<endl;

	 double brakePercentFront = BrakePower(DistanceFront);//calling function of calculate brake power
	 //cout<<"____Front Obstacle Information___"<<endl;
	 //cout<<"There are obstacle in front of the car with distance "<<DistanceFront<<"m."<<endl;
	 //if(brakePercentFront>0){
	 cout<<"Brake power force: "<<brakePercentFront<<endl;//}

	 double accePercentBack = AccePower(DistanceBack);//calling function of calculate accelerator power
	 //cout<<"____Back Obstacle Information___"<<endl;
	 //cout<<"There are obstacle in back of the car with distance "<<DistanceBack<<"m."<<endl;

	 cout<<"acce power force:"<<accePercentBack<<endl;
	 v_obsDetect.brakePedal=brakePercentFront;
	 v_obsDetect.accelPedal=accePercentBack;


}
