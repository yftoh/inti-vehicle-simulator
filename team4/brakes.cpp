/*
 * main.cpp
 *
 *  Created on: 31 May, 2014
 *      Author: n45sf
 */

#include "libsub.h"
#include <stdio.h>
#include <iostream>
#include <math.h>
#include "brakes.h"
#include <string.h>
using namespace std;

int brake_pwm(int duty);
int e_brake_pwm();
static int brake_location();
double brake_PF();
static unsigned int grayToBinary(unsigned int num);
int brake_motor_direction(int x);
int brake_init();
static int get_brake_handle();

static sub_device dev=0;
static sub_handle handle=0;
const struct sub_version* sub_ver;
char buf[32];

int get;

//dimax initialization
int get_brake_handle()
{
	while(1)
	{
		dev=sub_find_devices(dev);
		if(!dev)
		{
			printf("No sub-20 device found.");
			return 0;
		}
		handle=sub_open(dev);
		if(!handle)
		{
			printf("sub_open: %s\n",sub_strerror(sub_errno));
			return 0;
		}
		if(sub_get_serial_number(handle,buf,sizeof(buf))>=0)
		{
			//check if the serial number matches
			if(strcmp(buf,"0F22") == 0)
			{
				break; //successfully found, handle is retained.
			}
			printf("Serial Number: %s\n",buf);
		}
		else
		{
			printf("Error in obtaining serial.");
			return 0;
		}
		sub_close(handle);
	}

	if(sub_get_product_id(handle,buf,sizeof(buf))>=0)
		printf("Product ID: %s\n",buf);
	else
	{
		printf("Error in obtaining product ID!\n");
		return 0;
	}
	return 0;
}

//Brake initialization
int brake_init()
{
	get_brake_handle();
	sub_gpio_config(handle,0x0100F010,&get,0xFFFFFFFF);
	return 0;
}

//GPIO to set DC direction
int brake_motor_direction(int x)
{
	if (x==1)
		sub_gpio_write(handle,0x00000010,&get,0x00000010);
	else
		sub_gpio_write(handle,0x00000000,&get,0x00000010);
	return 0;
}
//GPIO24 settings for DC motor to pull brakes, duty ranges from 0-255.
int brake_pwm(int duty)
{
	sub_pwm_config(handle,400,255);
	sub_pwm_set(handle,0,duty);
	return 0;
}

//emergency brake algorithm
int e_brake_pwm()
{
	sub_pwm_config(handle,400,255);
	sub_pwm_set(handle,0,255);
	brake_location();
	return 0;
}

//brake position read
static int brake_location()
{
	int brake_position;
	sub_gpio_read(handle,&brake_position);
	brake_position=(brake_position>>12)&0b1111;
	brake_position=grayToBinary(brake_position);
	return brake_position;
}

//brake PF
double brake_PF()
{
	int brake_position;
	double max, current, power_factor;
	max=exp(1.5)-1;
	brake_position=brake_location();
	current=exp(brake_position/10.0)-1;
	power_factor=current/max;
	return power_factor;
}

//converting gray code to binary
static unsigned int grayToBinary(unsigned int num)
{
    unsigned int mask;
    for (mask = num >> 1; mask != 0; mask = mask >> 1)
    {
    	num = num ^ mask;
    }
    return num;
}
