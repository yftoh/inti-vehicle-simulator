#ifndef BRAKES_H_
#define BRAKES_H_

int brake_pwm(int duty);		//adjustable PWM for brake pulling motors.
int e_brake_pwm();				//e-braking 100 duty cycle PWM
double brake_PF();				//to get PF from brake encoder
int brake_motor_direction(int x);//to control brake DC motor moving direction
int brake_init();				//to initialize braking system

#endif
