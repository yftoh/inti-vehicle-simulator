#include "cv.h"
#include "highgui.h"
#include<iostream>
#include <opencv2/core/core.hpp>
#include <cstring>
#include <math.h>
using namespace std;

#include "vehicleModelClass.h"
#include "Obstacles.h"

// initialize global variables
//int g_slider_position = 0; // trackbar position
CvCapture* g_capture = NULL; // structure to create a video input

// routine to be called when user moves a trackbar slider
/*void onTrackbarSlide(int pos) {
  cvSetCaptureProperty(
  g_capture,
  CV_CAP_PROP_POS_FRAMES,//set the slider value to equal to frame position

  pos
  );
}*/




/*int main(int argc,char** argv ) {
// create a window with appropriate size. Windows name is determined by file name
// supplied as an argument
cvNamedWindow( argv[1], 0 );
// open video
//cvResizeWindow(argv[1],1920,1080);
g_capture = cvCreateFileCapture( argv[1] );
//argv[1]
// set read position in units of frames and retrieve total number of frames
int frames = (int) cvGetCaptureProperty(

  g_capture,
  CV_CAP_PROP_FRAME_COUNT
);

int framerate=(int)cvGetCaptureProperty(g_capture,CV_CAP_PROP_FPS);
cout<<argv[0]<<endl;
// do not create treackbar if video does not include an information
// about number of frames
/*if( frames!=0 ) {

  cvCreateTrackbar(
"Position",
argv[1],
  &g_slider_position,
  frames,

  onTrackbarSlide
  );
}*/

// display video frame by frame
/*double x=0;
double y=0;
IplImage* frame;
while(1) {

frame = cvQueryFrame(g_capture );

if( !frame ) break;


int FrameNum=cvGetCaptureProperty(g_capture,CV_CAP_PROP_POS_FRAMES);
cvShowImage( argv[1], frame );
// set trackbar to a current frame position
//cvSetTrackbarPos("Position", argv[1], g_slider_position);
cout<<"FrameNum";
cout<<FrameNum<<endl;
cout<<"x=";
cout<<x<<endl;
cout<<"y=";
cout<<y<<endl;
x=x+0.34;
//g_slider_position++;
char c = cvWaitKey(20);
//char c = cvWaitKey(0);
// quit if ESC is pressed
if( c == 27 ) break;
system("clear");
}

// free memory
cvReleaseCapture( &g_capture );
cvDestroyWindow( argv[1] );
return(0);
}*/

IplImage* coordinate(double x, double y)
{
	IplImage* FrmAdd=0;

	int FrameNum= 0;
	int CurrentFrm;
	int SkipFrm;

	CurrentFrm=cvGetCaptureProperty(g_capture,CV_CAP_PROP_POS_FRAMES);
	FrameNum=1+((x/1582.8)*2638);
	SkipFrm=FrameNum-CurrentFrm;
	cout<<"skipped:"<<SkipFrm<<endl;
	for(int i=0;i<SkipFrm;i++)
	{
		FrmAdd=cvQueryFrame(g_capture);

	}

	cout<<"x="<<x<<endl;
	cout<<"y="<<y<<endl;
	return FrmAdd;



}

void delay()
{
	for(int i=0;i<3000;i++);

}

char videoFile[200];
char stringbuffer[200];
void displayInit(char vFile[])
{
	//strcpy(videoFile,vFile);
	// create a window with appropriate size. Windows name is determined by file name
	// supplied as an argument
	cvNamedWindow( vFile, 0);
	strcpy(videoFile,vFile);
	// open video
	cvResizeWindow(vFile,1366,800);
	g_capture = cvCreateFileCapture( vFile );
	//argv[1]
	// set read position in units of frames and retrieve total number of frames
	int totalframes = (int) cvGetCaptureProperty(

	  g_capture,
	  CV_CAP_PROP_FRAME_COUNT
	);

	cout<<"totalframes"<<totalframes<<endl;
	int framerate=(int)cvGetCaptureProperty(g_capture,CV_CAP_PROP_FPS);
	cout<<"framerate"<<framerate<<endl;



//	IplImage* frame;
//	for(int i=0;i<200;i++)
//	{
//		frame=cvQueryFrame(g_capture);
//			if( !frame ) return;
//
//			//int FrameNum=cvGetCaptureProperty(g_capture,CV_CAP_PROP_POS_FRAMES);
//			cvShowImage( videoFile, frame );
//			int FrameNumber=cvGetCaptureProperty(g_capture,CV_CAP_PROP_POS_FRAMES);
//			// set trackbar to a current frame position
//			cout<<"FrameNumber"<<FrameNumber<<endl;
//			char c = cvWaitKey(10);
//	}
}

void displayFrame(Vehicle v)
{
	// display video frame by frame
//	IplImage* frame2;
//	do {
//		frame2=cvQueryFrame(g_capture);
//		if(frame2)
//			cvShowImage(videoFile,frame2);
//		cvWaitKey(33);
//	}while(frame2);

	IplImage* frame=coordinate(v.x,v.y);
	if( !frame ) return;
	//displayObstacles(frame,v);
	int XPixelShift;
	int NewXpixel;
	XPixelShift=(v.y/4)*660;
	NewXpixel=440-XPixelShift;
	if(NewXpixel<0)
		NewXpixel=0;
	if(NewXpixel > 880)
		NewXpixel=880;
	//int FrameNum=cvGetCaptureProperty(g_capture,CV_CAP_PROP_POS_FRAMES);
	int FrameNumber=cvGetCaptureProperty(g_capture,CV_CAP_PROP_POS_FRAMES);
	cout<<"FrameNumber"<<FrameNumber<<endl;
	//cvCircle(frame, cvPoint(500,200), 30, CV_RGB(0,255,0), -1, 8);
	//cvLine(frame, cvPoint(30,100), cvPoint(300,500), CV_RGB(255,0,0), 1, 8 );

	displayObstacles(frame,v);
	CvRect crop=cvRect(NewXpixel,200,400,400);
	cvSetImageROI(frame,crop);

	//draw text string on :
	CvPoint textPos;

	CvFont fnt;
	cvInitFont(&fnt,CV_FONT_HERSHEY_SIMPLEX,0.3,0.4,0,1,8);
	textPos=cvPoint(280,395);
	sprintf(stringbuffer,"X:%3.1f  Y:%3.1f",v.x,v.y);
	cvPutText(frame,stringbuffer,textPos,&fnt, CV_RGB(255,50,50));
	textPos=cvPoint(280,370);
	sprintf(stringbuffer,"Speed:%.1fm/s",v.velocity,v.velocity*3.6);
	cvPutText(frame,stringbuffer,textPos,&fnt, CV_RGB(50,50,255));
	textPos=cvPoint(280,382);
	sprintf(stringbuffer,"%.0fkm/h",v.velocity*3.6);
	cvPutText(frame,stringbuffer,textPos,&fnt, CV_RGB(50,50,255));
	cvShowImage( videoFile, frame );

	// set trackbar to a current frame position



//	//char c = cvWaitKey(1000/framerate);
	cvWaitKey(1);

//	// quit if ESC is pressed
double width=cvGetCaptureProperty(g_capture,CV_CAP_PROP_FRAME_WIDTH);
double height =cvGetCaptureProperty(g_capture,CV_CAP_PROP_FRAME_HEIGHT);
cout<<"width:"<<width<<endl;
cout<<"height"<<height<<endl;
//	if( c == 27 ) break;
	system("clear");

}
