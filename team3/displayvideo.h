/*
 * displayvideo.h
 *
 *  Created on: Jun 27, 2014
 *      Author: fiona
 */

#ifndef DISPLAYVIDEO_H_
#define DISPLAYVIDEO_H_
#include "vehicleModelClass.h"

void coordinate(double x, double y);
void displayInit(char* vFile);
void displayFrame(Vehicle v);


#endif /* DISPLAYVIDEO_H_ */
