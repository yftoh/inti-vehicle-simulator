#include <math.h>
#include <stdio.h>
#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "vehicleModelClass.h"
#include "pointClass.h"
using namespace std;

#include "Obstacles.h"

double bearingList[5];				//heading angle from the vehicle to all obstacles
double distanceObs[5];					//calculate the distance of the obstacle and the vehicle
void updateObstacleList(Vehicle& v)
{
	static Point obs[5];//create five Point objects to store obstacles

	obs[0].setPoint(800,10);			//1st obstacle
	obs[1].setPoint(600,-15);			//2nd obstacle
	obs[2].setPoint(720,20);
	obs[3].setPoint(1500,1);
	obs[4].setPoint(900,0);

	v.obstacleNum=5; 				//total five obstacles set
	v.obstacleList= & (obs[0]);
}

//function to display the specified obstacle which is nearby the vehicle
//input parameters:vehicle heading, vector from vehicle to obstacle, and bearing
void displayObstacles(IplImage* img, Vehicle v)
{
	const double videoWidth=1280;			 	//width of the video frame
	const double videoHeight=720;			 	//height of the video frame
	const double actualVideoHeight=780/2;  		//height of the video frame excluding the undesired part(blue sky)
	const double factorX=2;						//factor to determine x pixel
	double angleDiff;
	double pixelX;
	double meterPerPixel;
	double pixelY;
	double radiusObs;

	for(int i=0;i<5;i++)
	{
		angleDiff=bearingList[i]-v.heading;						//calculate the angle which the obstacle tilts away from the vehicle
		pixelX=(videoWidth/2)-(angleDiff*factorX);				//specify the x pixel to draw the obstacle
		meterPerPixel=50/videoHeight;							//calculate the road distance per pixel
		pixelY=videoHeight-((distanceObs[i]/meterPerPixel)/2);	//specify the y pixel to draw the obstacle
		radiusObs=((50-distanceObs[i])/50)*50;					//specify the size of the obstacle based on the obstacle distance
		//img = cvCreateImage(cvSize(videoWidth,videoHeight),8,3);		//create an image header to allocate the image data

		//draw the obstacle only,
		//when the pixel x is more than 0 and less than video width
		//when the pixel y is more than actual video height and less than the video height
		if(pixelX>0 &&  pixelX<1280 && pixelY>actualVideoHeight && pixelY<videoHeight && distanceObs[i]<50.0 && abs(bearingList[i])<10.0/180*3.14159265)
		{
			//draw filled circle at specified radius correspond to the obstacle distance at specified pixel x and y
			cvCircle(img, cvPoint(pixelX,pixelY), radiusObs, CV_RGB(0,255,0), -1, 8);
			cvLine(img, cvPoint(videoWidth/2,videoHeight), cvPoint(pixelX,pixelY), CV_RGB(255,0,0), 1, 8 );
			//create a opencv window named img
			//cvNamedWindow("Output", CV_WINDOW_AUTOSIZE);
			//display obstacles on the image
			//cvShowImage("/home/fiona/Videos/IMG_5675_cut_baseline2.mp4", img);
			//cvWaitKey(0);
			//cvReleaseImage(&img);
			//cvDestroyWindow("Output");
		}
	}

}

//function to calculate the position between the vehicle and the obstacles
void calcEnvironmentModel(Vehicle &v)
{
	double vectorX=0;			//vector x from vehicle to obstacle
	double vectorY=0;			//vector y from vehicle to obstacle
	Point* obs=v.obstacleList;	//store set obstacles in a pointer
	for(int i=0;i<5;i++)
	{
		vectorX=obs[i].x-v.x;		//calculate the vector x from the vehicle to the obstacle
		vectorY=obs[i].y-v.y;		//calculate the vector y from the vehicle to the obstacle
		bearingList[i]=atan2 (vectorY,vectorX);		//calculate the arc tangent of vector from vehicle to obstacle
		distanceObs[i]=sqrt(vectorX*vectorX+vectorY*vectorY);
		//distance between vehicle and obstacle
		//plus minus 10 degree from the vehicle heading
		if(abs(distanceObs[i])<1 && abs(v.heading-bearingList[i])<0.1745)	//check whether the vehicle collides the obstacle
		{
			v.velocity=0;											//stop the vehicle from moving
			v.acceleration=0;
			//v.heading=v.heading-0.087;								//the vehicle tilts by 5 degree after collision
			v.x=obs[i].x-(vectorX/2);								//the vehicle goes backward
			v.y=obs[i].y-(vectorY/2);
		}
		//do nothing if no possible collision happens
	}
}

Point getRoadStartPos() //defines the starting coordinate of the road
{
	static Point P;
	P.x=0;
	P.y=0;
	return P;
}

Point getRoadEndPos() //defines the ending coordinate of the road
{
	static Point P;
	P.x=200; //TODO: EDIT THIS VALUE TO SUIT YOUR IMPLEMENTATION/VIDEO LENGTH
	P.y=0;
	return P;
}



