/*
 * Obstacles.h
 *
 *  Created on: Jul 10, 2014
 *      Author: fiona
 */

#ifndef OBSTACLES_H_
#define OBSTACLES_H_
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "vehicleModelClass.h"
#include "pointClass.h"

void updateObstacleList(Vehicle& v);
void displayObstacles(IplImage* img, Vehicle v);
void calcEnvironmentModel(Vehicle &v);
Point getRoadStartPos();
Point getRoadEndPos();



#endif /* OBSTACLES_H_ */
