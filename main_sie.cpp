/*
 * main_sie.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: tyf
 */
/* this is the main function for the vehicle sim */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <linux/hpet.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <cmath>
#include <iostream>

#include "vehicleModelClass.h"
#include "VM.h" //from QuahChinBeng
#include "LaneObjectTrack.h"
#include "ObstacleDetect.h"
#include "DecisionControl.h"
//#include <cv.h>
#include "matlab_capture.h"

#include "team3/Obstacles.h"
#include "team3/displayvideo.h"

using namespace std;

#define SAMPLEFREQ 40 // 40Hz
#define SAMPLETIME 25 //sampling time = 25ms
#define MAFPOINTS 4 //4 Points moving-average filter

//#define MATLABDEBUG //when this is uncommented, it compiles the matlab data capture code

int interrupt_count=0;

static void hpet_alarm(int val) //the SIGIO signal handler function
{
	interrupt_count++; //count the number of interrupts occured.
	return;
}

int main()
{
	bool stop=false;
	float xPos = 0; //initial position for x
	float yPos = 0; //initial position for y
	float heading = 0; //initial heading angle for car
	float mass = 1045; // mass of the vios according to vios specification
	float length = 4.41; // according to the vios specification
	float width = 1.7; // according to the vios specification
	float height = 1.475; // according to the vios specification
	float ROW = 0.302; //according to 185/60R15 tyre's specification. ROW = (inner radius+outer radius) /2
	float vMax = 47.22; //maximum speed of vios = 170km / h which is 47.22 m/s
	float Tengine = 141; //according to vios specification
	float gearRatio = 0.7;
	float finalGearRatio = 4.24;

	Vehicle v(xPos, yPos,heading,mass,length,width,height,ROW,vMax,Tengine,gearRatio,finalGearRatio,2.55,1.46); // initialize the vios specification,you can use other car specification as well
	Vehicle v_laneDetect;
	Vehicle v_obsDetect;
	double steerAng; //steering angle applied by user, in Radians
	double brakePdl; //brake pedal applied by user
	double accPdl;//accelerator pedal applied by user

	double avgAccPdl=0, avgBrakePdl=0,avgSteerAng=0; // persistent variable for performing filtering

	//set up the periodic timer
	struct sigaction oldsig, newsig; //stores the signal handling settings of this program
	struct hpet_info info; //for obtaining info of the HPET hardware
	int    frequency;
	int    retval = 0;
	int    fd;
	int    r, value;
	char buf[100];

	frequency = SAMPLEFREQ; //in Hz

#ifndef MATLABDEBUG
	//Elevate the scheduling policy and priority of the program:
	sprintf(buf,"pkexec chmod a+r /dev/hpet"); //make the HPET hardware readable by normal users
	system(buf);
	sprintf(buf,"pkexec chrt -r -p 40 %d ",getpid()); //use Round-robin scheduling, sched priority=5
	system(buf);//execute the command above

	sigemptyset(&newsig.sa_mask); //create empty signal set in 'newsig'
	newsig.sa_flags = 0;
	newsig.sa_handler = hpet_alarm;  //set the SIGIO signal handler function

	sigaction(SIGIO,&newsig, &oldsig); // assign new action for SIGIO=new, store the oldsig action to 'oldsig'

	fd = open("/dev/hpet", O_RDONLY); //open the HPET device read-only
	if (fd < 0) {
		cerr<<"ERROR: Failed to open /dev/hpet\n";
		fprintf(stderr,"Make sure you have read permission on this device!");
		return -1;
	}

	if ((fcntl(fd, F_SETOWN, getpid()) == 1) ||  //set owner equal to this process
		((value = fcntl(fd, F_GETFL)) == 1) ||    //Get the file access mode and the file status flags
		(fcntl(fd, F_SETFL, value | O_ASYNC) == 1)) { //Set the file status flags to the value specified by arg, for sending SIGIO
		cerr<<"ERROR: fcntl failed\n";
		retval = 1;
		goto fail;
	}

	if (ioctl(fd, HPET_IRQFREQ, frequency) < 0) { //check whether the frequency is allowed
		cerr<<"ERROR: Could not set /dev/hpet to have a " <<frequency <<" Hz timer\n";
		retval = 2;
		goto fail;
	}

	if (ioctl(fd, HPET_INFO, &info) < 0) { //obtain the info struct about the HPET timer
		cerr<< "ERROR: failed to get info\n";
		retval = 3;
		goto fail;
	}

	sprintf(buf,"\nhi_ireqfreq: 0x%lx  hi_flags: 0x%lx  hi_hpet: 0x%x  hi_timer: 0x%x\n\n",
			info.hi_ireqfreq,  info.hi_flags, info.hi_hpet, info.hi_timer);
	cout<<buf; //print info of the HPET hardware

	r = ioctl(fd, HPET_EPI, 0);  //enable periodic timer
	if (info.hi_flags && (r < 0)) {
		cerr<<"ERROR:  HPET_EPI failed\n";
		retval = 4;
		goto fail;
	}

	if (ioctl(fd, HPET_IE_ON, 0) < 0) { //turn on HPET Interrupt
		cerr<<"ERROR: HPET_IE_ON failed\n";
		retval = 5;
		goto fail;
	}

#endif
	//call other team's Dimax initialization functions
	/*
	 * init_brake(); //inside each of their function, they find their own dimax devices
	 * init_accelerate();
	 * init_steering();
	 */
	displayInit("/home/tyf/IMG_5675_cut_baseline2.mp4");
	v.x=50;//the video is not moving at first two seconds
	updateObstacleList(v);

	struct timespec t;
	double nanosec_prev;
	double nanosec;

	clock_gettime(CLOCK_REALTIME, &t);//get current time
	nanosec_prev=(t.tv_sec)*1000000000.0 + t.tv_nsec*1.0;

	initDataCapture();//start matlab data capture

	// start the sampling loop
#ifndef MATLABDEBUG
	while(stop==false && interrupt_count<2000) //run until 2000 counts only!
#else
	for(int count=0;count<2000;count++)//Increase count from 800 to 2000
#endif
	{
#ifndef MATLABDEBUG
		pause();// wait for the SIGIO signal //DEBUG NOTE: COMMENT during MATLAB analysis
#endif
		//cout<<"count: "<<interrupt_count<<endl;

		//read from sensors
		//call the functions that read sensors from other 3 teams
		/*
		 * steerWheelAngle=read_steering();
		 * accPedalAmount=read_acc_pedal();
		 * brkPedalAmount=read_brk_pedal();
		 *
		 */
		//dummy sensor readings:
		steerAng=0.5/180.0*3.14159265;
		brakePdl=0;
		accPdl=0.8;

		//if(v.brakePedal>0.1) brakePdl=v.brakePedal; //simulate the application of brake assist

		//if(v.accelPedal>0.1) accPdl=v.accelPedal;

#ifdef MATLABDEBUG

		if(count>1400)
			accPdl=0; //stop applying accelerator at 3s

		if(count>1800)
			brakePdl=0.5;

		if(count>50)
			steerAng=v.steeringAngle;//override user here
#else
		if(interrupt_count>800)
			accPdl=0; //stop applying accelerator at 3s

		if(interrupt_count>5000)
			brakePdl=0.5;

		if(interrupt_count>50)
			steerAng=v.steeringAngle;//override user here
#endif
		v.userAccel=accPdl;
		v.userBrake=brakePdl;
		v.userSteer=steerAng;
		// Moving average filters to filter sensor noise, if required.
		avgAccPdl=avgAccPdl*(MAFPOINTS-1) + accPdl;
		avgAccPdl/=MAFPOINTS;

		avgBrakePdl=avgBrakePdl*(MAFPOINTS-1) + brakePdl;
		avgBrakePdl/=MAFPOINTS;

		avgSteerAng=avgSteerAng*(MAFPOINTS-1) + steerAng;
		avgSteerAng/=MAFPOINTS;


		// pass the data to the data-processing functions
		//calculate Vehicle model:
		calcVehicleModel(accPdl,brakePdl,steerAng,v);
		//calculate environment model:
	    calcEnvironmentModel(v);
		// calculate the amount of the actuator control based on new vehicle parameters
		/* Lane tracking requires: nearest road in the map, list of obstacles around it
		 * Huiwen may need to create a function that is able to:
		 * 	given the distance r, return all the obstacles that are within r meters away from the vehicle's current position.
		 * 	given the car's current position, return the nearest road-vertex on the map.
		 *
		 */
	    calcLaneTrack(v,v_laneDetect);
	    calcObstacleDetection(v,v_obsDetect);

	    v.steeringAngle=steerAng; //pass the user applied values to the vehicle model
	    v.accelPedal=accPdl;	// because decision control needs to use it.
	    v.brakePedal=brakePdl;

	    decisionControl(v,v_laneDetect,v_obsDetect);

		 /*
		 * applyAccel(v.accelPedal);
		 * applyBrake(v.brakePedal);
		 * applySteer(v.steeringAngle);
		 *
		 */


		//	generate new frame for display
		 displayFrame(v); //TODO DEBUG


	    //capture data for analysis in matlab:
	    captureData(v);

	}

	clock_gettime(CLOCK_REALTIME, &t);//get current time
	nanosec=(t.tv_sec)*1000000000.0 + t.tv_nsec*1.0;

	cout<<"Time diff: "<<(nanosec-nanosec_prev)/1000000.0<<" milisecs\n";


	endCaptureData(v);


    if (ioctl(fd, HPET_IE_OFF, 0) < 0) { //turn off HPET interrupt
        cout<<"ERROR: HPET_IE_OFF failed\n";
        retval = 6;
    }


fail:
	sigaction(SIGIO, &oldsig, NULL); //restore the old signal action

	if (fd > 0)   //close if opened
		close(fd);

	if(retval==0) cout<<"Program ended successfully\n";

	return 0;
}
