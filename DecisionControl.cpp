#include <iostream>
#include <cmath>
#include <math.h>
#include "vehicleModelClass.h"
#include "LaneObjectTrack.h"
#include "ObstacleDetect.h"
#include "DecisionControl.h"
using namespace std;

void decisionControl(Vehicle &v, Vehicle &v_lanetrack,Vehicle &v_obsDetect){
	double steeringAngle=0;
	double brakePower=0;
	double accelerator=0;

	steeringAngle=v_lanetrack.steeringAngle;//preload steering angle of lanetrack

	brakePower=v_lanetrack.brakePedal;//preload brake power of lanetrack
	if(v_obsDetect.brakePedal>brakePower){//if obs decided brake power  bigger
		brakePower=v_obsDetect.brakePedal;//decided brake power  is obsdetect
	}

	accelerator=v_obsDetect.accelPedal;

	//decision actuator control is stored into vehicle v class
	v.steeringAngle=steeringAngle;//direct apply steering calculated from lane object tracking system

	if(brakePower>v.brakePedal){//if control brake power more than user applied brake power
		v.brakePedal=brakePower;
	}

	if(brakePower>=0.1){//when brake is applied, accelerator cannot be applied at the same timee
		v.accelPedal=0;
	}
	else{
		//if(accelerator>v.accelPedal)//if obstacle team applied accelerator more than driver apply
			v.accelPedal=accelerator;
	}
}
