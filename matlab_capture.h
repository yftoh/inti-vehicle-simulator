/*
 * matlab_capture.h
 *
 *  Created on: Jul 8, 2014
 *      Author: tyf
 */

#ifndef MATLAB_CAPTURE_H_
#define MATLAB_CAPTURE_H_

#include "vehicleModelClass.h"

void initDataCapture();
bool fileExist(char* filename);
void captureData(Vehicle v);
void endCaptureData(Vehicle v);

#endif /* MATLAB_CAPTURE_H_ */
