/*
 * matlab_capture.cpp
 *
 *  Created on: Jul 8, 2014
 *      Author: tyf
 */

#include <fstream>
#include <iostream>
#include <cstdio>

#include <vehicleModelClass.h>
#include "matlab_capture.h"

using namespace std;



ofstream outFile;

void initDataCapture()
{
	char strBuf[30];
	int i=0;
	//check if a filename exists
	for( i=0;i<1000;i++)
	{
		sprintf(strBuf,"datafile%03d.m",i);
		if(!fileExist(strBuf))
			break;

	}
	cout<<"Writing to "<<strBuf<<endl;
	outFile.open(strBuf,ios::app);
	//start by writing some matlab comments:
	outFile<<"%% x position, y position, appliedBrake, appliedAccel, appliedSteer, velocity\n";
	outFile<<"data=[ \n";
}


void captureData(Vehicle v)
{
	//data analysis in matlab:
	outFile<<v.x<<" "<<v.y<<" "<<v.brakePedal<<" "<<v.accelPedal<<" "<<v.steeringAngle
			<<" "<<v.velocity<<" "<<v.userAccel<< " "<<v.userBrake <<endl;
}

inline bool fileExist (char* filename) {
    fstream f(filename);
    return f.good();
}


void endCaptureData(Vehicle v)
{//finish up by writing some matlab code
	outFile<<"];\n";
	outFile<<"time=linspace(0,length(data)/40,length(data)); %data is captured at 40Hz \n"
			"time=data(:,1);\n" //replace with x-location first...
	"figure(1)\n"
	"subplot(4,1,1)\n"
	"plot(time,data(:,4),'b',time,data(:,7),'r')\n"
	"legend('driver-assist','user-applied')\n"
	"title('accel pedal amount')\n"
	"subplot(4,1,2)\n"
	"plot(time, data(:,3),'b',time,data(:,8),'r')\n"
	"legend('driver-assist','user-applied')\n"
	"title('brake pedal amount')\n"
	"subplot(4,1,3)\n"
	"plot(time, data(:,1),'r',time, data(:,2),'b')\n"
	"title ('position x and y')\n"
	"legend('x','y')\n"
    "subplot(4,1,4)\n"
	"plot(time, data(:,6),'g')\n"
	"title('velocity') \n"

	"figure(2)\n"
	"plot(time,data(:,5))\n"
	"title('steering vs distance')\n"

	"figure(3) \n"
	"plot(data(:,1), data(:,2),'r')\n"
	"title('x-y plot')\n";
	//plot the obstacles
	outFile<<"hold on\n";
	for(int i=0;i<v.obstacleNum;i++)
	{
		outFile<<"text("<<v.obstacleList[i].x
				<<","<<v.obstacleList[i].y
				<<",'"<<i<<"','color','red','edgecolor',[1 0 .2])\n";
	}
	outFile<<"hold off\n";
	outFile.close();
}
