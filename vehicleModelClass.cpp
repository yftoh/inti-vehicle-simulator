/*
 * vehicleModelClass.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: tyf and QCB
 */

#include "vehicleModelClass.h"

Vehicle::Vehicle()
{
	x=y=0;
	heading=0;
	accelPedal=0;
	brakePedal=0;
	steeringAngle=0;

	mass=0;
	length=0;
	width=0;
	height=0;
	ROW=0;
	vMax = 0;
	acceleration=0;
	gearRatio=0;
	finalGearRatio=0;
	ROW=0;
	Tengine=0;
	fbMax=0;
	WB=0;
	track=0;
	velocity=0;

	obstacleList=0;//set pointer to 0
	obstacleNum=0;
}


//Toh Yi Feng
Vehicle::Vehicle(double xPos, double yPos,double headingAngle)
{
	x=xPos;
	y=yPos;
	heading=headingAngle;
	accelPedal=0;
	brakePedal=0;
	steeringAngle=0;

	mass=0;
	acceleration=0;
	velocity=0;

	obstacleList=0;
	obstacleNum=0;
}

//Quah Chin Beng
Vehicle::Vehicle(double xPos, double yPos,double headingAngle,double mass1,float length1,double width1,float height1,
		float ROW1,float vMax1,float Tengine1,float gearRatio1,float finalGearRatio1,float WB1,float track1)
{
	x=xPos;
	y=yPos;
	heading=headingAngle;
	mass = mass1;
	length = length1;
	width = width1;
	height =height1;
	ROW = ROW1;
	vMax = vMax1;
	Tengine = Tengine1;
	gearRatio = gearRatio1;
	finalGearRatio = finalGearRatio1;
	track = track1;
	WB = WB1;
	velocity = 0;

	obstacleList=0;
	obstacleNum=0;
}
