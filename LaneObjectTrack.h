/*
 * LaneObjectTrack.h
 *
 *  Created on: Jun 26, 2014
 *      Author: soonzhi
 */

#ifndef LANEOBJECTTRACK_H_
#define LANEOBJECTTRACK_H_

void calcLaneTrack(Vehicle &v, Vehicle &v_lanetrack);

#endif /* LANEOBJECTTRACK_H_ */
