/*
 * VM.h
 *
 *  Created on: Jun 11, 2014
 *      Author: john
 */

#ifndef VM_H_
#define VM_H_
#include "vehicleModelClass.h"

float tAccelerator(float acceleratorPosition);
float tFriction(float velocity);
float tBrake(float pressure);
void calcVehicleModel(float acceleratorPosition,float brakePedalPosition,float steeringAngle,Vehicle &v);

#endif /* VM_H_ */
